# Masterani Downloader

This is an automatic downloader for ther Masterani.me website that can download entire series.

# Usage

Simply run the script with the following parameters

./MasterAniDl.sh [Url of anime, minus the /(episode #)] [folder to save to]

ex: masteranidl https://www.masterani.me/anime/watch/3007-kaze-ga-tsuyoku-fuiteiru /home/alex/Videos