#!/bin/bash
#
# Masterani downloader written by SpectralMemories
# (aaalex12gagne@gmail.com)
# On October 23 2018
# 
# Allow an automatic download of entire anime series from Masterani.me
# See https://gitlab.com/SpectralMemories/masterani-downloader for more info
#
# Licensed under the MIT license. See Gitlab page for full license
#
# This script comes with absolutely no warranty. Feel free to report any
# Bugs or suggestions in the gitlab issues

if [ "$1" == "--help" ]
then
    echo "Downloads a serie on masterani"
    echo "MasterAniDl.sh [url of serie] [folder to download to] [options]"
    echo "Options are:"
    echo "-v | --verbose for more stdout logs"
    echo "--startfrom=X to start from episode X instead of 1 [inclusive]"
    echo "--onlydownload=X to only download episode X instead of whole series"
    echo "--stopat=X to stop downloading at X [exclusive]"
    exit 0
fi

if [ "$1" == "" ]
then
    echo "Error: you must specify a series url from masterani to download"
    echo "This url is as follow: https://www.masterani.me/anime/watch/[XYZ]"
    exit 1
fi

if [ "$2" == "" ]
then
    echo "Error: you must specify a directory do download to"
    exit 3
fi

SoloEpisodeNo=0
EpisodeStartFrom=1
StopAt=1000000

echo "Validating URL..."
confirmation=$(curl -s --head "$1/1" | grep 'HTTP')
confirmation=$(echo $confirmation | grep -E -o "[0-9]+" | tail -1)

if [ "$confirmation" != "200" ]
then
    echo "Error: $1 was not found"
    exit 2
fi

echo "URL is valid!"



Url="$1"
RootFolder="$2"

verbose=false

while test $# -gt 0
do
    case "$1" in
        -v|--verbose)
            echo "Now running in verbose mode";verbose=true
            ;;
    esac
    case "$1" in 
        --startfrom=*)
            EpisodeStartFrom=${1:12};
            echo "Starting from episode $EpisodeStartFrom";
            ;;
    esac
    case "$1" in
        --onlydownload=*)
            SoloEpisodeNo=${1:15};
            echo "Only downloading $SoloEpisodeNo";
            ;;
    esac
    case "$1" in
        --stopat=*)
            StopAt=${1:9};
            echo "Download will stop at episode $StopAt"
            ;;
    esac
    shift
done



# Parameters: direct Url [string], folder [string], filename [string]
# Depends: 
DownloadEpisode () {
    url="$1"
    folder="$2"
    file="$3"
    path="$folder/$file"

    wget -q --show-progress -O "$path" "$url"
}

# Parameters: mp4Upload Url [string]
# Depends:
# Returns: the direct Url of an episode [string]
IsolateDownloadUrl () {
    mp4uUrl="$1"
    linkName=${mp4uUrl:28}
    templateUrl="https://www.tubeoffline.com/downloadFrom.php?host=MP4upload&video=https%3A%2F%2Fmp4upload.com%2F"
    html=$(wget -qO- "$templateUrl$linkName" | grep -oe 'https://.*.mp4upload.com.*/video.mp4' | sed 's/.*"//')

    echo $html
}

currentEpisode=$EpisodeStartFrom

if (($SoloEpisodeNo > 0))
then
    echo "Fetching direct download URL for episode $SoloEpisodeNo..."
    masteraniUrl="$Url/$SoloEpisodeNo"
    
    processedHtml=$(phantomjs "/usr/share/masterani/GetProcessedHtml.js" "$masteraniUrl" | grep -oe "https://mp4upload.com/embed.*.html"| sed 's/".*//')
    
    if [ "$verbose" = true ]
    then
        echo "DEBUG: got mp4url of $processedHtml"
    fi
    directUrl=$(IsolateDownloadUrl "$processedHtml")

    if [ "$verbose" = true ]
    then
        echo "DEBUG: got direct Url of $directUrl"
    fi
    if [ "$directUrl" == "" ]
    then
        echo "Warning: direct url of episode $SoloEpisodeNo could not be fetched. Aborting..."
        exit 4
    else
        echo "Found it!"
    fi
    echo "Downloading episode $SoloEpisodeNo..."
    DownloadEpisode "$directUrl" "$RootFolder" "$SoloEpisodeNo.mp4"

    echo "Episode $SoloEpisodeNo downloaded!"
    exit 0
fi
# Loop through the episodes
while (($currentEpisode < $StopAt))
do

    #Check if episode exists
    confirmation=$(curl -s --head "$Url/$currentEpisode" | grep 'HTTP')
    confirmation=$(echo $confirmation | grep -E -o "[0-9]+" | tail -1)

    if [ ! "$confirmation" == "200" ]
    then
        echo "Finished downloading!"
        break
    fi

    echo "Fetching direct download URL for episode $currentEpisode..."
    masteraniUrl="$Url/$currentEpisode"
    
    processedHtml=$(phantomjs "/usr/share/masterani/GetProcessedHtml.js" "$masteraniUrl" | grep -oe "https://mp4upload.com/embed.*.html"| sed 's/".*//')
    
    if [ "$verbose" = true ]
    then
        echo "DEBUG: got mp4url of $processedHtml"
    fi
    directUrl=$(IsolateDownloadUrl "$processedHtml")

    if [ "$verbose" = true ]
    then
        echo "DEBUG: got direct Url of $directUrl"
    fi
    if [ "$directUrl" == "" ]
    then
        echo "Warning: direct url of episode $currentEpisode could not be fetched. Skipping..."
        currentEpisode=$(($currentEpisode + 1))
        continue
    else
        echo "Found it!"
    fi
    echo "Downloading episode $currentEpisode..."
    DownloadEpisode "$directUrl" "$RootFolder" "$currentEpisode.mp4"

    echo "Episode $currentEpisode downloaded!"

    currentEpisode=$(($currentEpisode + 1))
done