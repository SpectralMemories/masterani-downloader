phantom.cookiesEnabled = true;

phantom.clearCookies ();

var viewportSize = {
    width: 1280,
    height: 918
}


var page = require('webpage').create();
var fs = require('fs');
var system = require('system');
var url = system.args[1];
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36';
page.viewportSize = viewportSize;
page.zoomFactor = 1;
page.cookiesEnabled = true;
page.clearCookies();

page.addCookie({
    'name'     : 'pref_host',   /* required property */
    'value'    : '1',  /* required property, required to request mp4upload */
    'domain'   : 'www.masterani.me',
    'path'     : '/',                /* required property */
    'httponly' : false,
    'secure'   : false,
    'expires'  : (new Date()).getTime() + (1000 * 60 * 60)   /* <-- expires in 1 hour */
});

page.addCookie({
    'name'     : 'pref_mirror',   /* required property */
    'value'    : '2%3B1080',  /* required property. Dubbed by default*/
    'domain'   : 'www.masterani.me',
    'path'     : '/',                /* required property */
    'httponly' : false,
    'secure'   : false,
    'expires'  : (new Date()).getTime() + (1000 * 60 * 60)   /* <-- expires in 1 hour */
});

page.onLoadFinished = function () {
    //fs.write("/home/alex/Documents/result.html", page.content, 'w');
    console.log(page.content);
    phantom.exit();
}

page.open(url, function () {
});

